import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import './vendor';
import { MultitenantSharedModule } from 'app/shared/shared.module';
import { MultitenantCoreModule } from 'app/core/core.module';
import { MultitenantAppRoutingModule } from './app-routing.module';
import { MultitenantHomeModule } from './home/home.module';
import { MultitenantEntityModule } from './entities/entity.module';
// jhipster-needle-angular-add-module-import JHipster will add new module here
import { MainComponent } from './layouts/main/main.component';
import { NavbarComponent } from './layouts/navbar/navbar.component';
import { FooterComponent } from './layouts/footer/footer.component';
import { PageRibbonComponent } from './layouts/profiles/page-ribbon.component';
import { ErrorComponent } from './layouts/error/error.component';

@NgModule({
  imports: [
    BrowserModule,
    MultitenantSharedModule,
    MultitenantCoreModule,
    MultitenantHomeModule,
    // jhipster-needle-angular-add-module JHipster will add new module here
    MultitenantEntityModule,
    MultitenantAppRoutingModule,
  ],
  declarations: [MainComponent, NavbarComponent, ErrorComponent, PageRibbonComponent, FooterComponent],
  bootstrap: [MainComponent],
})
export class MultitenantAppModule {}
