/**
 * View Models used by Spring MVC REST controllers.
 */
package com.javalearn.web.rest.vm;
