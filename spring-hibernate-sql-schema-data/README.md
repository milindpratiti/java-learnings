# SQL Schema creation and intiail Data load

Spring data with hibernate supports automatic creation of database tables based on the entity definition. 
The creation/update of database tables is controlled through variable spring.jpa.hibernate.ddl-auto. It can have values like 'none','update', 'create',  'create-drop', 'validate'.

1. 'none' : will not do any modifications to database tables/schema
2. 'update': will modify the table structure based on changes in corresponding entity classes
3. 'create': will create new tables and columns based on the corresponding entity definition classes
4. 'create-drop' : will create tables and columns based on the corresponding entity definition classes and drop them when the SessionFactory is explicityl closed or typically when application closes.

Typicall we keep the value as update which will take care of creation and updation both.

## Data load
There may be a requirement to populate the database tables with an initial set of data, say master data used by the application.
Spring JPA has support to populate the initial data using standard SQL statements. This statements are saved in file with name 'data.sql' and the file should be available in classpath.
The variable from application.yml which controls the behavior of data load is 

 `spring.datasource.initialization-mode = always`
 
To handle the differences in sql syntax for different databases like Oracle Postgres, DB2, MSSQL, Mysql, we can created different data files named as data-${platform}.sql. The platform variable is defined as
  `spring.datasource.platform = mysql`
Corresponding data.sql file name needs to be changed to data-mysql.sql

complete configuration file will look like
 
```
spring:
  datasource:
    url: jdbc:mysql://localhost:3306/test_db?useSSL=false&&createDatabaseIfNotExist=true
    **platform: mysql**
    username: <USER>
    password: <PWD>
    **initialization-mode: always**
  jpa:
    database-platform: org.hibernate.dialect.MySQL5InnoDBDialect
    database: MYSQL
    show_sql: true
    hibernate:
      **ddl-auto: update**
```



## Schema Creation/ Manipulation

Spring also allows to maintain the database schema using file schema.sql ( schema-mysql.sql or schema-postgresql.sql for platform specific files).
Table creation statements are added to this file. When schema.sql is used, the configuration hibernate.ddl-auto should be set to 'none'. So that changes in entity classes does not affect the database schema.


### Spring boot application ( Spring-hibernate-sql-schema-data) demonstrates use of data.sql and schema.sql
