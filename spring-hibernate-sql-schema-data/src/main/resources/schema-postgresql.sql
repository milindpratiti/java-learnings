  
CREATE TABLE IF NOT EXISTS "user" (
  "user_id" SERIAL PRIMARY KEY,
  "email" varchar(255) DEFAULT NULL,
  "first_name" varchar(255) DEFAULT NULL,
  "last_name" varchar(255) DEFAULT NULL,
  "designation"	varchar(255) DEFAULT NULL
) ;