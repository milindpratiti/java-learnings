package com.milindbarve.SpringData1.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.milindbarve.SpringData1.domain.User;

@Repository
public interface UserRepository extends JpaRepository<User, Long>{
	
	public List<User> findAll();
	public User findOneByUserId(Long id);

}
